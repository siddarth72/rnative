import React, { useState } from 'react';
import {
	View,
	SafeAreaView,
	ScrollView,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from './Screens/Home.jsx';
import Register from './Screens/Register.jsx';

const stack = createNativeStackNavigator();

export default function App() {

	return (
		<NavigationContainer>
			<stack.Navigator initialRouteName='Home'>
				<stack.Screen
					name="Home"
					component={Home}
					// options={{ headerStyle: { backgroundColor: 'wheat' } }}
					options={{ headerShown: false }}
				/>
				<stack.Screen
					name="Register"
					component={Register}
					options={{ headerStyle: { backgroundColor: 'wheat' } }}
				/>
			</stack.Navigator>
		</NavigationContainer>
	);
};

