import { View, ToastAndroid, Text, StyleSheet } from 'react-native';
import React, { useState } from 'react';
import { TextInput, Button, Checkbox } from 'react-native-paper';
import SafeAreaViewAndroid from '../Components/SafeAreaViewAndroid';

const Main = () => {

	const [checked, setChecked] = useState(false);

	const showToast = () => {
		ToastAndroid.show('You have successfully registered!', ToastAndroid.SHORT);
	};

	return (
		<View>
			<TextInput style={styles.inputs} placeholder="Fullname" />
			<TextInput style={styles.inputs} placeholder="Email" />
			<TextInput style={styles.inputs} placeholder="Password" secureTextEntry={true} />
			<View
				style={{
					alignSelf: 'center',
					flexDirection: 'row',
					alignItems: 'center',
				}}
			>
				<Checkbox
					status={checked ? 'checked' : 'unchecked'}
					onPress={() => setChecked(!checked)}
				/>
				<Text>Please accept all terms and conditions</Text>
			</View>
			<Button
				style={styles.btn}
				disabled={!checked}
				onPress={showToast}
			>
				Register
			</Button>
		</View>
	);
};

const Register = ({ navigation }) => {
	return (
		<SafeAreaViewAndroid Component={Main} navigation={navigation} />
	);
};

export default Register;

const styles = StyleSheet.create({
	inputs: {
		backgroundColor: 'white',
		marginHorizontal: 50,
		marginVertical: 10,
	},
	btn: {
		backgroundColor: 'skyblue',
		color: 'purple',
		marginHorizontal: 80,
		marginVertical: 15,
	}
})
