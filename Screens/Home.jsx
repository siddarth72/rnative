import { View, Text, TouchableOpacity } from 'react-native';
import React from 'react';
import {
	Button,
	Appbar,
	Avatar,
	Headline,
} from 'react-native-paper';
import SafeAreaViewAndroid from '../Components/SafeAreaViewAndroid';

const Main = ({ navigation }) => {
	return (
		<View style={{
			backgroundColor: 'white',
		}}>
			<Appbar style={{
				backgroundColor: 'pink',
			}}>
				<Appbar.Action icon="menu" onPress={() => console.log('MENU')} />
				<Appbar.Content title="Home" />
				<Appbar.Action icon="account-circle-outline" onPress={() => console.log('MENU')} />
				<Appbar.Action icon="camera" onPress={() => console.log('MENU')} />
			</Appbar>
			<TouchableOpacity>
				<Avatar.Image
					source={{ uri: "https://images.pexels.com/photos/10069874/pexels-photo-10069874.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" }}
					size={160}
					style={{ alignSelf: 'center', margin: 40 }}
				/>
			</TouchableOpacity>
			<Button
				color="white"
				style={{ backgroundColor: "blue", marginHorizontal: 60, borderRadius: 30 }}
				onPress={() => navigation.navigate('Register')}
			>
				HOME
			</Button>
			<Headline style={{ textAlign: 'center', marginTop: 20 }}>
				About Me
			</Headline>
			<Text style={{ textAlign: 'center', width: "50%", alignSelf: 'center', marginTop: 20 }}>
				Hi, I am Siddarth B A, Full stack MERN developer...
			</Text>
		</View >
	);
};

const Home = ({ navigation }) => {
	return (
		<SafeAreaViewAndroid Component={Main} navigation={navigation} />
	);
};

export default Home;

